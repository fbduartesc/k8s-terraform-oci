## Passo a passo

Baixar o projeto no github [Projeto](https://github.com/jpetazzo/ampernetacle)

Ter uma conta no oracle cloud - always free - ate 4 VM 24GB de ram.

Clonar o projeto - no meu caso ja tenho este repo no meu gitlab.

Se não quiser instalar o terraform, pode usar com o docker.

```sh
user@linux:~$ docker container run -it -v $PWD:/app -w /app --entrypoint "" hashicorp/terraform:light sh
```

## Instalando kubeadm, kubelet and kubectl

```sh
user@linux:~$ sudo apt-get update
user@linux:~$ sudo apt-get install -y apt-transport-https ca-certificates curl
user@linux:~$ sudo curl -fsSLo /usr/share/keyrings/kubernetes-archive-keyring.gpg https://packages.cloud.google.com/apt/doc/apt-key.gpg
user@linux:~$ echo "deb [signed-by=/usr/share/keyrings/kubernetes-archive-keyring.gpg] https://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee /etc/apt/sources.list.d/kubernetes.list
user@linux:~$ sudo apt-get update
user@linux:~$ sudo apt-get install -y kubelet kubeadm kubectl
user@linux:~$ sudo apt-mark hold kubelet kubeadm kubectl
```

## Instalando o Terraform

```sh
user@linux:~$ sudo apt-get update && sudo apt-get install -y gnupg software-properties-common curl
user@linux:~$ curl -fsSL https://apt.releases.hashicorp.com/gpg | sudo apt-key add -
user@linux:~$ sudo apt-add-repository "deb [arch=amd64] https://apt.releases.hashicorp.com $(lsb_release -cs) main"
user@linux:~$ sudo apt-get update && sudo apt-get install terraform
user@linux:~$ terraform version
```

## Instalar OCI CLI

```sh
user@linux:~$ bash -c "$(curl -L https://raw.githubusercontent.com/oracle/oci-cli/master/scripts/install/install.sh)"
```

Depois de instalar os pacotes, vamos mover o binário oci para dentro do **/usr/local/bin** para que possamos usar sem nenhum problema.

```sh
user@linux:~$ sudo cp /home/<USER>/bin/oci /usr/local/bin
```

## Agora falta se autenticar no oci

```sh
user@linux:~$ oci session authenticate
user@linux:~$ terraform init
user@linux:~$ terraform plan
user@linux:~$ terraform apply
user@linux:~$ export KUBECONFIG=$PWD/kubeconfig
```

## Outra forma de deixar a configuracao definitiva, é copiar esse kubeconfig para dentro do **.kube**

```sh
user@linux:~$ cp kubeconfig /home/<USER>/.kube/config
user@linux:~$ kubectl get nodes
```

Agora já está pronto e pode rodar o que quiser no cluster.
Para destruir as instâncias, basta executar **terraform destroy**

```sh
user@linux:~$ terraform destroy
```

Se ocorrer o erro **Error: 401-NotAuthenticated**, remover o arquivo de config do oci.

```sh
user@linux:~$ rm ~/.oci/config
```

Após removido o arquivo é só se autenticar novamente.

```sh
user@linux:~$ oci session authenticate
user@linux:~$ terraform destroy
```
